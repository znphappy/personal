import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class mm {
	public static void main(String a[]) throws FileNotFoundException, IOException {

		// 准备配置文件
		Properties p0 = new Properties();
		p0.load(new FileInputStream("211806401.properties"));
		Enumeration<?> fileName = p0.propertyNames();
		
		
		// 获取配置文件的键值
		double ScoreBefore = Integer.parseInt(p0.getProperty("before"));
		double ScoreBase = Integer.parseInt(p0.getProperty("base"));
		double ScoreTest = Integer.parseInt(p0.getProperty("test"));
		double ScoreProgram = Integer.parseInt(p0.getProperty("program"));
		double ScoreAdd = Integer.parseInt(p0.getProperty("add"));
		
		
		
		// 导入大小班课活动html文件
		File file_small = new File("small.html");
		File file_all = new File("all.html");

		getScores(file_small, file_all, ScoreBefore, ScoreBase, ScoreTest, ScoreProgram, ScoreAdd);

	}

	
	
	public static void getScores(File small_File, File all_File, double ScoreBefore, double ScoreBase, double ScoreTest,
			double ScoreProgram, double ScoreAdd) throws IOException {

		double my_Before = 0;
		double my_Base = 0;
		double my_Test = 0;
		double my_Program = 0;
		double my_Add = 0;

		org.jsoup.nodes.Document document = Jsoup.parse(small_File, "UTF-8");
		org.jsoup.nodes.Document document1 = Jsoup.parse(all_File, "UTF-8");

		double AllBefore = my_Before / ScoreBefore * 100 * 0.25;
		double AllBase = my_Base / ScoreBase * 100 * 0.3 * 0.95;
		double AllTest = my_Test / ScoreTest * 100 * 0.2;
		double AllProgram = my_Program / ScoreProgram * 100 * 0.1;
		double AllAdd = my_Add / ScoreAdd * 100 * 0.05;
		double AllScore = (AllBefore + AllBase + AllTest + AllProgram + AllAdd) + 6;
		System.out.println(AllScore);
	}

}

